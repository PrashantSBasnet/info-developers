package com.info.employee_details.service;


import com.info.employee_details.dto.DesignationDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DesignationService {

     DesignationDto saveDesignation (DesignationDto designationDto);

     List<DesignationDto> findAll();

     DesignationDto findById (Integer id);

     void deleteDesignationById(Integer id);

     //get designation by keywords
     List<DesignationDto> findByKeyword(String keyword);

     List<DesignationDto> findByStatus(String keyword);
}
