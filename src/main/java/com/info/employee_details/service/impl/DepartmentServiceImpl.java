package com.info.employee_details.service.impl;

import com.info.employee_details.dto.DepartmentDto;
import com.info.employee_details.entity.Department;
import com.info.employee_details.repo.DepartmentRepo;
import com.info.employee_details.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

    @Slf4j
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepo departmentRepo;


    @Override
    public List<DepartmentDto> getAllDepartments() {
        List<DepartmentDto> departmentDtoList = new ArrayList<>();
        List<Department> departments = departmentRepo.getAllDepartment();
        for(Department d: departments){
            departmentDtoList.add(new DepartmentDto(d.getId(),d.getDepartmentName(),d.getDepCode(),
                    d.getIsActive(),d.getContactPerson(),d.getContactNumber()));
        }
        return departmentDtoList;
//        return departments.stream().map(d ->
//                DepartmentDto.builder()
//                        .id(d.getId())
//                        .departmentName(d.getDepartmentName())
//                        .depCode(d.getDepCode())
//                        .isActive(d.getIsActive())
//                        .contactPerson(d.getContactPerson())
//                        .contactNumber(d.getContactNumber())
//                        .build()).collect(Collectors.toList());
    }

    @Override
    public DepartmentDto saveDepartment(DepartmentDto departmentDto) throws Exception {
        Department department = new Department();
//        department.setIsActive(true);
        try {
            Optional<Department> optionalDepartment = departmentRepo.findByCode(departmentDto.getDepCode());
            if (optionalDepartment.isPresent() && departmentDto.getId()==null) {
                throw new Exception("Department with this code already exist.");
            }
            department.setId(departmentDto.getId());
            department.setDepartmentName(departmentDto.getDepartmentName());
            department.setDepCode(departmentDto.getDepCode());
            department.setIsActive(departmentDto.getIsActive());
            department.setContactPerson(departmentDto.getContactPerson());
            department.setContactNumber(departmentDto.getContactNumber());
            department= departmentRepo.save(department);
        } catch (Exception exception) {
            throw new Exception(exception.getMessage());
        }
        return DepartmentDto.builder()
                .id(department.getId())
                .departmentName(department.getDepartmentName())
                .depCode(department.getDepCode())
                .isActive(department.getIsActive())
                .contactPerson(department.getContactPerson())
                .contactNumber(department.getContactNumber())
                .build();
    }

    @Override
    public DepartmentDto getDepartmentById(Integer id) {
        Department department;
        Optional<Department> optionalDepartment = departmentRepo.getOneDepartment(id);
        if (optionalDepartment.isPresent()){
            department = optionalDepartment.get();
            return DepartmentDto.builder()
                    .id(department.getId())
                    .departmentName(department.getDepartmentName())
                    .depCode(department.getDepCode())
                    .isActive(department.getIsActive())
                    .contactPerson(department.getContactPerson())
                    .contactNumber(department.getContactNumber())
                    .build();
        }
        return null;
    }

    @Override
    public void deleteDepartmentById(Integer id) {
        departmentRepo.deleteById(id);
    }



    /**
     * below is old one working fine
     */

//    @Override
//    public List<Department> getAllDepartments() {
//
//        return departmentRepo.getAllDepartment();
//    }
//
//    @Override
//    public void saveDepartment(DepartmentDto departmentDto) throws Exception {
//        Department department = new Department();
////        department.setIsActive(true);
//        try {
//            Optional<Department> optionalDepartment = departmentRepo.findByCode(departmentDto.getDepCode());
//            if (optionalDepartment.isPresent()) {
//                throw new Exception("Department with this code already exist.");
//            }
//            department.setDepartmentName(departmentDto.getDepartmentName());
//            department.setDepCode(departmentDto.getDepCode());
//            department.setIsActive(true);
//            departmentRepo.save(department);
//        } catch (Exception exception) {
//            throw new Exception(exception.getMessage());
//        }
//    }
//
//    @Override
//    public DepartmentDto getDepartmentById(Integer id) throws Exception {
//        Department department = departmentRepo.getOneDepartment(id);
//
//        DepartmentDto departmentDto = new DepartmentDto(departmentRepo.getOneDepartment(id));
////        Department department = null;
////        if(optional.isPresent()){
////            department = optional.get();
////        }else {
////            throw new RuntimeException("Department not found for id:: "+id);
////        }
//        if (department.getId() == null) {
//            throw new Exception("Department ID is null");
//        }
//        return departmentDto;
//    }
//
//    @Override
//    public void deleteDepartmentById(Integer id) {
//
//        Department department = departmentRepo.getOneDepartment(id);
//        department.setIsActive(false);
//        departmentRepo.saveAndFlush(department);
////         departmentRepo.deleteById(id);
//    }
}
