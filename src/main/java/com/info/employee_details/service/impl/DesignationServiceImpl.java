package com.info.employee_details.service.impl;

import com.info.employee_details.dto.DesignationDto;
import com.info.employee_details.entity.Designation;
import com.info.employee_details.repo.DesignationRepository;
import com.info.employee_details.service.DesignationService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@Service
@RequiredArgsConstructor
public class DesignationServiceImpl implements DesignationService {

    @Autowired
    private DesignationRepository designationRepository;


    @Override
    public DesignationDto saveDesignation(DesignationDto designationDto){

        Designation entity = new Designation();
        entity.setId(designationDto.getId());
        entity.setName(designationDto.getName());
        entity.setShortcode(designationDto.getShortcode());
        entity.setStatus(designationDto.getStatus());
        entity= designationRepository.save(entity);

        return DesignationDto.builder().
                id(entity.getId())
                .name(entity.getName())
                .shortcode(entity.getShortcode())
                .status(entity.getStatus())
                .build();
    }



    @Override
    public List<DesignationDto> findAll() {

        List<DesignationDto> ddto = new ArrayList<>();
        List<Designation> designations = designationRepository.findAll();

//        for (Designation d : designations){
//            ddto.add(new DesignationDto(d.getId(), d.getName(), d.getShortcode(), d.getStatus()));
//        }

        //new way using builder
        return designations.stream().map(d ->
                DesignationDto.builder().
                        id(d.getId()).
                        name(d.getName()).
                        shortcode(d.getShortcode()).
                        status(d.getStatus())
                        .build()).collect(Collectors.toList());
    }



    @Override
    public DesignationDto findById(Integer id) {
        //optional is used to prevent null pointer exception
        //new feature in java
        Designation deg;
        Optional<Designation> optionalDesignation = designationRepository.findById(id);

        if (optionalDesignation.isPresent()){
            deg = optionalDesignation.get();
            return DesignationDto.builder().
                    id(deg.getId())
                    .name(deg.getName())
                    .shortcode(deg.getShortcode())
                    .status(deg.getStatus())
                    .build();
        }
        return  null;
    }



    @Override
    public void deleteDesignationById(Integer id) {
        this.designationRepository.deleteById(id);
    }



    //get designation by keyword
    @Override
    public List<DesignationDto> findByKeyword(String keyword) {
        String searchWord = "%" + keyword + "%";
        List<Designation> designationsearch = designationRepository.findByKeyword(searchWord);

        return designationsearch.stream().map(d ->
                DesignationDto.builder().
                        id(d.getId()).
                        name(d.getName()).
                        shortcode(d.getShortcode()).
                        status(d.getStatus())
                        .build()).collect(Collectors.toList());
    }

    @Override
    public List<DesignationDto> findByStatus(String keyword) {
        Boolean searchWord =  keyword.equalsIgnoreCase("true") ? true : false;
        List<Designation> designationsearch = designationRepository.findByStatus(searchWord);

        return designationsearch.stream().map(d ->
                DesignationDto.builder().
                        id(d.getId()).
                        name(d.getName()).
                        shortcode(d.getShortcode()).
                        status(d.getStatus())
                        .build()).collect(Collectors.toList());
    }
}
