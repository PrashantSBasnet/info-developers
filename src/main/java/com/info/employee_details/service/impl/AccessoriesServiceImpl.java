package com.info.employee_details.service.impl;

import com.info.employee_details.dto.AccessoriesDto;
import com.info.employee_details.entity.Accessories;
import com.info.employee_details.repo.AccessoriesRepo;
import com.info.employee_details.service.AccessoriesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AccessoriesServiceImpl implements AccessoriesService {

    @Autowired
    private AccessoriesRepo accessoriesRepo;

    @Override
    public List<AccessoriesDto> getAllAccessories() {
        List<AccessoriesDto> accessoriesDtoList = new ArrayList<>();
        List<Accessories> accessories = accessoriesRepo.getAllAccessories();
        for (Accessories a: accessories) {
            accessoriesDtoList.add(new AccessoriesDto(
                    a.getId(),a.getAccessoriesName(),a.getSerialKey(),a.getStatus(),
                    a.getAccessoriesTypeEnum(),a.getDescription(),a.getIsActive()));
        }
        return accessoriesDtoList;
    }

    @Override
    public AccessoriesDto saveAccessories(AccessoriesDto accessoriesDto) {
        Accessories accessories = new Accessories();

        accessories.setId(accessoriesDto.getId());
        accessories.setAccessoriesName(accessoriesDto.getAccessoriesName());
//        accessories.setAccessoriesType(accessoriesDto.getAccessoriesType());
//        accessories.setQuantity(accessoriesDto.getQuantity());
        accessories.setSerialKey(accessoriesDto.getSerialKey());
        accessories.setStatus(accessoriesDto.getStatus());
        accessories.setAccessoriesTypeEnum(accessoriesDto.getAccessoriesEnum());
        accessories.setDescription(accessoriesDto.getDescription());
        accessories.setIsActive(true);
        accessories = accessoriesRepo.save(accessories);


        return AccessoriesDto.builder()
                .id(accessories.getId())
                .accessoriesName(accessories.getAccessoriesName())
//                .accessoriesType(accessories.getAccessoriesType())
//                .quantity(accessories.getQuantity())
                .serialKey(accessories.getSerialKey())
                .status(accessories.getStatus())
                .accessoriesEnum(accessories.getAccessoriesTypeEnum())
                .description(accessories.getDescription())
                .isActive(accessories.getIsActive())
                .build();
    }

    @Override
    public AccessoriesDto getAccessoriesById(Integer id) {
        Accessories accessories;
        Optional<Accessories> optionalAccessories = accessoriesRepo.getOneAccessories(id);
        if(optionalAccessories.isPresent()){
            accessories=optionalAccessories.get();

            return AccessoriesDto.builder()
                    .id(accessories.getId())
                    .accessoriesName(accessories.getAccessoriesName())
//                    .accessoriesType(accessories.getAccessoriesType())
//                    .quantity(accessories.getQuantity())
                    .serialKey(accessories.getSerialKey())
                    .status(accessories.getStatus())
                    .accessoriesEnum(accessories.getAccessoriesTypeEnum())
                    .description(accessories.getDescription())
                    .isActive(accessories.getIsActive())
                    .build();
        }
        return null;
    }

    @Override
    public void deleteAccessoriesById(Integer id) {
        Accessories accessories = accessoriesRepo.getById(id);
        accessories.setIsActive(false);
        accessoriesRepo.saveAndFlush(accessories);
    }

    @Override
    public List<AccessoriesDto> getUnique() {
        List<Tuple> datas = accessoriesRepo.getUnique();
        if(datas.size() >0){
            List<AccessoriesDto> accessoriesDtoList = datas.stream().map(tuple -> {
                return   new AccessoriesDto().builder()
                        .id((Integer) tuple.get("id"))
                        .accessoriesName((String) tuple.get("name"))
                        .serialKey((String) tuple.get("serialKey"))
                        .status((Boolean) tuple.get("status"))
                        .description((String) tuple.get("description"))
                        .build();
            } ).collect(Collectors.toList());
            return accessoriesDtoList;
        }
        return null;
    }
}
