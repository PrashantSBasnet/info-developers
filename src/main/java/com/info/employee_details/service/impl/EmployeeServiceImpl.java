package com.info.employee_details.service.impl;

import com.info.employee_details.dto.EmployeeDto;
import com.info.employee_details.entity.Accessories;
import com.info.employee_details.entity.Department;
import com.info.employee_details.entity.Employee;
import com.info.employee_details.repo.AccessoriesRepo;
import com.info.employee_details.repo.DepartmentRepo;
import com.info.employee_details.repo.EmployeeRepo;
import com.info.employee_details.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepo employeeRepo;

    @Autowired
    private DepartmentRepo departmentRepo;

    @Autowired
    private AccessoriesRepo accessoriesRepo;

    @Override
    public EmployeeDto saveEmployee(EmployeeDto employeeDto) {

        Integer departmentId = employeeDto.getDepartmentId();
        Optional<Department> optionalDepartment = departmentRepo.getOneDepartment(departmentId);
        if (!optionalDepartment.isPresent()){
            log.info("Department with given id is not present.");
            return null;
        }

        List<Integer> accessoriesIdList = employeeDto.getAccessoriesIdList();
        List<Accessories> accessoriesList = accessoriesRepo.findAllById(accessoriesIdList);

        Employee employee = Employee.builder()
                .id(employeeDto.getId())
                .fullName(employeeDto.getFullName())
                .address(employeeDto.getAddress())
                .emailId(employeeDto.getEmailId())
                .phoneNo(employeeDto.getPhoneNo())
                .age(employeeDto.getAge())
                .department(optionalDepartment.get())
                .accessoriesList(accessoriesList).build();
        employee= employeeRepo.save(employee);

        return EmployeeDto.builder().id(employee.getId()).build();
    }

    @Override
    public List<EmployeeDto> findAllEmployees() {
        List<EmployeeDto> employeeDtos = new ArrayList<>();
        List<Employee> employees = employeeRepo.getAllEmployee();
        for (Employee e:employees) {
            employeeDtos.add(new EmployeeDto(e.getId(),e.getFullName(),e.getAddress(),
                    e.getEmailId(),e.getPhoneNo(),
                    e.getAge(),e.getDepartment(),e.getAccessoriesList()));
        }
        return employeeDtos;
    }

    @Override
    public EmployeeDto findById(Integer id) {
        return null;
    }




//    @Autowired
//    private EmployeeRepo employeeRepo;
//
//    @Override
//    public List<Employee> getAllEmployees(){
//
//        return employeeRepo.findAll();
//    }
//
//    @Override
//    public void saveEmployee(Employee employee){
//        this.employeeRepo.save(employee);
//    }
//
//    @Override
//    public Employee getEmployeeById(Integer id) {
//        Optional<Employee> optional = employeeRepo.findById(id);
//        Employee employee = null;
//        if(optional.isPresent()){
//            employee = optional.get();
//        }else {
//            throw new RuntimeException("Employee not found for id:: "+id);
//        }
//        return employee;
//    }
//
//    @Override
//    public void deleteEmployeeById(Integer id) {
//
//        this.employeeRepo.deleteById(id);
//    }


}
