package com.info.employee_details.service;

import com.info.employee_details.dto.DepartmentDto;
import com.info.employee_details.entity.Department;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDto> getAllDepartments();
    DepartmentDto saveDepartment(DepartmentDto departmentDto) throws Exception;
    DepartmentDto getDepartmentById(Integer id);
    void deleteDepartmentById(Integer id);
}
