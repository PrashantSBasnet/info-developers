package com.info.employee_details.service;

import com.info.employee_details.dto.EmployeeDto;
import com.info.employee_details.entity.Employee;

import java.util.List;

public interface EmployeeService  {

    EmployeeDto saveEmployee(EmployeeDto employeeDto);
    List<EmployeeDto> findAllEmployees();
    EmployeeDto findById(Integer id);
//    List<Employee> getAllEmployees();
//    void saveEmployee(Employee employee);
//    Employee getEmployeeById(Integer id);
//    void deleteEmployeeById(Integer id);
}
