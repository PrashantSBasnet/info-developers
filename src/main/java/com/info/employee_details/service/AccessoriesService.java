package com.info.employee_details.service;

import com.info.employee_details.dto.AccessoriesDto;
import com.info.employee_details.entity.Accessories;

import java.util.List;

public interface AccessoriesService {
    List<AccessoriesDto> getAllAccessories();
    AccessoriesDto saveAccessories(AccessoriesDto accessoriesDto);
    AccessoriesDto getAccessoriesById(Integer id);
    void deleteAccessoriesById(Integer id);
    List<AccessoriesDto> getUnique();

}
