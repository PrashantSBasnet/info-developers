package com.info.employee_details.repo;

import com.info.employee_details.entity.Accessories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

public interface AccessoriesRepo extends JpaRepository<Accessories,Integer> {

    @Query(
            nativeQuery = true,
            value = "select * from accessories_details "
    )
    List<Accessories> getAllAccessories();

    @Query(
            nativeQuery = true,
            value="select * from accessories_details where accessories_id = ?1 "
    )
    Optional<Accessories> getOneAccessories(Integer id);

    @Query(
            nativeQuery = true,
            value = "select * from accessories_details ad where ad.accessories_id = ?1 "
    )Accessories findByCode(String code);

    @Query(nativeQuery = true,
            value = "select ad.accessories_id as id, ad.accessories_name as name, ad.accessories_serial_key as serialKey, ad.status as status, ad.description as description\n" +
                    "from accessories_details ad where ad.is_active = true and  ad.accessories_id not in\n" +
                    "(select eam.accessories_list_accessories_id from employee_accessories_mapping eam)")
    List<Tuple> getUnique();

    @Modifying
    @Query(
            nativeQuery = true,
            value = "delete  * from  accessories_details where  id  =?1"
    )
    void delete(Integer id);
}
