package com.info.employee_details.repo;

import com.info.employee_details.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {

    @Query(
            nativeQuery = true,
            value = "select * from employee_details"
    )
    List<Employee> getAllEmployee();
}
