package com.info.employee_details.repo;


import com.info.employee_details.entity.Designation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface DesignationRepository extends JpaRepository<Designation, Integer> {
    //TODO: queries must be changed.
    @Modifying
    @Query(value= "UPDATE tbl_designation" +
            " SET STATUS = false where status = true", nativeQuery = true)
    @Transactional
    void deleteById(Integer id);


    @Query(value="select * from tbl_designation d where d.name like ?1", nativeQuery=true)
    List<Designation> findByKeyword(String keyword);

    @Query(value="select * from tbl_designation d where d.status=?1", nativeQuery=true)
    List<Designation> findByStatus(Boolean keyword);



}
