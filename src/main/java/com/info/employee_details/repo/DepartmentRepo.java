package com.info.employee_details.repo;

import com.info.employee_details.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface DepartmentRepo extends JpaRepository<Department, Integer> {

    @Query(
            nativeQuery = true,
            value = "select * from department_details order by department_name asc"
    )
    List<Department> getAllDepartment();

    @Query(
            nativeQuery = true,
            value = "select * from department_details where id =?1"
    )
    Optional<Department> getOneDepartment(Integer id);

    @Query(
            nativeQuery = true,
          value = "select * from department_details dd where dd.department_code = ?1")
    Optional<Department> findByCode(String code);


    @Query(
            nativeQuery = true,
            value="update department_details set is_active=false where id=?1"
    )Void deleteDepartment(Integer id);
}


