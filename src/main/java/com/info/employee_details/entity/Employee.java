package com.info.employee_details.entity;


import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "employee_details")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "employee_full_name",length = 100)
    private String fullName;

    @Column(name ="employee_address", length = 100)
    private String address;

    @Column(name = "employee_emailId",length = 100)
    private String emailId;

    @Column(name = "employee_phoneNo" , length = 10)
    private String phoneNo;

    private Integer age;

    @ManyToOne(targetEntity = Department.class,fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id", foreignKey = @ForeignKey(name = "FK_EMPLOYEE_DEPARTMENTID"))
    private Department department;

    @ManyToMany(targetEntity = Accessories.class,fetch = FetchType.LAZY)
   @JoinTable(name = "employee_accessories_mapping",foreignKey = @ForeignKey(name = "FK_JT_EMPLOYEE_COURSE_MAPPING"))
    private List<Accessories> accessoriesList;

}
