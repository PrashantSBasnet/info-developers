package com.info.employee_details.entity;


import com.info.employee_details.enums.AccessoriesType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="accessories_details")
@AllArgsConstructor
@NoArgsConstructor
public class Accessories {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="accessories_id")
    private Integer id;

    @Column(name="accessories_name")
    private String accessoriesName;

//
//    private String accessoriesType;

//    @Column(name="accessories_quantity")
//    private String quantity;

    @Column(name = "accessories_serial_key")
    private String serialKey;

    private Boolean status;

    private String description;

    private Boolean isActive;

    @Enumerated(value = EnumType.STRING)
    @Column(name="accessories_type")
    private AccessoriesType accessoriesTypeEnum;
}
