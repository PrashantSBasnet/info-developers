package com.info.employee_details.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;

@Data
@Entity
@Table(name="tbl_designation")
@NoArgsConstructor
@AllArgsConstructor
public class Designation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name ="name")
    private String name;

    @Column(name="shortcode")
    private String shortcode;

    @Column (name="status")
    private Boolean status;

    //to make it bidirectional
    //   @OneToOne(targetEntity = Employee.class, mappedBy = "employee")
//    private Employee employee;


}
