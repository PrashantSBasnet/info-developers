package com.info.employee_details.entity;

import com.info.employee_details.dto.DepartmentDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "department_details")
@Table(name = "department_details",uniqueConstraints = {
        @UniqueConstraint(name="unique_dept",columnNames = "department_code")
})
public class Department {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name="department_code",nullable = false,unique = true)
    private String depCode;

    @Column(nullable = false,unique = true)
    private String departmentName;

    @Column(nullable = false,length = 100)
    private String contactPerson;

    @Column(nullable = false,length = 10)
    private String contactNumber;

    private Boolean isActive;

        @OneToMany(targetEntity = Employee.class,mappedBy = "department")
    private List<Employee> employeeList;

}
