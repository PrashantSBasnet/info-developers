package com.info.employee_details.controller;

import com.info.employee_details.dto.EmployeeDto;
import com.info.employee_details.entity.Employee;
import com.info.employee_details.service.AccessoriesService;
import com.info.employee_details.service.DepartmentService;
import com.info.employee_details.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private AccessoriesService accessoriesService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public String openEmployeeHomepage(Model model){
        if(model.getAttribute("employeeDto")==null) {
            model.addAttribute("employeeDto", new EmployeeDto());
        }
        model.addAttribute("departmentList",departmentService.getAllDepartments());
        model.addAttribute("accessoriesList",accessoriesService.getUnique());
        model.addAttribute("employeeDtoList",employeeService.findAllEmployees());

        return "employee/employee";
    }

    @PostMapping
    public String saveEmployee(@ModelAttribute EmployeeDto employeeDto, RedirectAttributes redirectAttributes){
        String message=" ";
        employeeDto = employeeService.saveEmployee(employeeDto);
        if (employeeDto==null){
            message="Employee Saved Failed.";
        }
        else    {
            message="Employee saved successfully.";
        }

        redirectAttributes.addFlashAttribute("message",message);
        return "redirect:/employee";
    }


//    private final EmployeeService employeeService;
//
//    //Display Employee List
//    @GetMapping("/employee")
//    public String viewEmployeeHomepage(Model model){
//        model.addAttribute("listEmployees",employeeService.getAllEmployees());
//        return "employeeHome";
//    }
//
//    @GetMapping("/showNewEmployeeForm")
//    public String showNewEmployeeForm(Model model){
//        Employee employee = new Employee();
//        model.addAttribute("employee",employee);
//        return "new_employee";
//    }
//
//    @PostMapping("/saveEmployee")
//    public String saveEmployee(@ModelAttribute("employee") Employee employee){
//        employeeService.saveEmployee(employee);
//        return "redirect:/employee";
//    }
//
//    @GetMapping("/showFormForUpdate/{id}")
//    public String showFormForUpdate(@PathVariable(value = "id") Integer id, Model model){
//        //get employee from the service
//        Employee employee = employeeService.getEmployeeById(id);
//
//        //set employee as a model attribute to pre-populate the form
//        model.addAttribute("employee",employee);
//        return "update_employee";
//    }
//
//    @GetMapping("/deleteEmployee/{id}")
//    public String deleteEmployee(@PathVariable(value = "id") Integer id){
//        //call delete employee method
//        this.employeeService.deleteEmployeeById(id);
//        return "redirect:/employee";
//    }
}
