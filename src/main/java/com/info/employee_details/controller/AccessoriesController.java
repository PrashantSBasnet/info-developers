package com.info.employee_details.controller;

import com.info.employee_details.dto.AccessoriesDto;
import com.info.employee_details.service.AccessoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/accessories")
public class AccessoriesController {
    @Autowired
    private AccessoriesService accessoriesService;

    @GetMapping
    public String openAccessoriesPage(Model model){
        if (model.getAttribute("accessoriesDto")==null) {
            model.addAttribute("accessoriesDto", new AccessoriesDto());
        }
        model.addAttribute("accessoriesDtoList", accessoriesService.getAllAccessories());
        return "accessories/accessories";
    }

    @PostMapping
    public String saveAccessories(@ModelAttribute AccessoriesDto accessoriesDto, RedirectAttributes redirectAttributes){
        String message = " ";
        accessoriesService.saveAccessories(accessoriesDto);
        if (accessoriesDto==null){
            message="Accessories cannot be saved!";
        }
        else {
            message="Accessories saved successfully.";
        }
        redirectAttributes.addFlashAttribute("message",message);
        redirectAttributes.addFlashAttribute("unique");
        return "redirect:/accessories";
    }


    @GetMapping("/find-by-id/{id}")
    public String findById(@PathVariable(value = "id") Integer id, RedirectAttributes redirectAttributes){
        AccessoriesDto accessoriesDto = accessoriesService.getAccessoriesById(id);
        if (accessoriesDto != null){
            redirectAttributes.addFlashAttribute("accessoriesDto",accessoriesDto);
        }
        return "redirect:/accessories";
    }

    @GetMapping("/delete/{id}")
    private String deleteAccessories(@PathVariable(value = "id") Integer id,RedirectAttributes redirectAttributes){
        String message= "Accessories Deleted Successfully ";
        accessoriesService.deleteAccessoriesById(id);
        redirectAttributes.addFlashAttribute("message",message);

        return "redirect:/accessories";
    }


}
