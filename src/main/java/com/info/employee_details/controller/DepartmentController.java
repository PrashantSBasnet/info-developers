package com.info.employee_details.controller;

import com.info.employee_details.dto.DepartmentDto;
import com.info.employee_details.entity.Department;
import com.info.employee_details.service.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequiredArgsConstructor
@RequestMapping("/department")
public class DepartmentController {

    private final DepartmentService departmentService;

    @GetMapping
    public String openDepartmentPage(Model model){
        if(model.getAttribute("departmentDto")==null) {
            model.addAttribute("departmentDto", new DepartmentDto());
        }
        model.addAttribute("departmentDtoList", departmentService.getAllDepartments());
        return "department";
    }

    @PostMapping
    public String saveDepartment(@ModelAttribute DepartmentDto departmentDto, RedirectAttributes redirectAttributes, Model model) throws Exception{
        //call a service to save the data
        try {
            String message=" ";
            departmentService.saveDepartment(departmentDto);
            if (departmentDto==null){
                message="Department Cannot be saved";
            }
            else{
                message="Department saved successfully.";
            }
            redirectAttributes.addFlashAttribute("message",message);
            return "redirect:/department";
        } catch (Exception exception) {
            redirectAttributes.addFlashAttribute("exception",exception.getMessage());
            //model.addAttribute("exception",exception.getMessage());
            return "redirect:/department";
        }
    }


    @GetMapping("/find-by-id/{id}")
    public String findById(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes){
        DepartmentDto departmentDto = departmentService.getDepartmentById(id);
        if (departmentDto != null) {
            redirectAttributes.addFlashAttribute("departmentDto", departmentDto);
        }
        return "redirect:/department";
    }


    @GetMapping("/delete/{id}")
    private String deleteDepartment(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes){
        String message = " Department deleted successfully";
        departmentService.deleteDepartmentById(id);
        redirectAttributes.addFlashAttribute("message",message);

        return "redirect:/department";
    }


    /**
     * Old one below working
     */

//    //Display Department List
//    @GetMapping("/department")
//    public String viewDepartmentHomepage(Model model) {
//        model.addAttribute("listDepartments", departmentService.getAllDepartments());
////        System.out.println(departmentService.getAllDepartments());
//
//        return "department_index";
//    }
//
//    @GetMapping("/showNewDepartmentForm")
//    public String showNewDepartmentForm(Model model) {
//        DepartmentDto departmentDto = new DepartmentDto();
//        model.addAttribute("departmentDto", departmentDto);
//        return "new_department";
//    }
//
//    @PostMapping("/saveDepartment")
//    public String saveDepartment(@ModelAttribute("departmentDto") DepartmentDto departmentDto,
//                                 Model model) throws Exception {
//        try {
//            departmentService.saveDepartment(departmentDto);
//            return "redirect:/department";
//        } catch (Exception exception) {
//            model.addAttribute("exception",exception.getMessage());
//            return "new_department";
//        }
//
//    }
//
//    @GetMapping("/showFormForDepartmentUpdate/{id}")
//    public String showFormForDepartmentUpdate(@PathVariable(value = "id") Integer id,
//                                              Model model) throws Exception {
//        //get department from the service
////        Department department = departmentService.getDepartmentById(id);
//
//        //set department as a model attribute to pre-populate the form
//        model.addAttribute("departmentDto", departmentService.getDepartmentById(id));
//        return "update_department";
//    }
//
//    @GetMapping("/deleteDepartment/{id}")
//    public String deleteDepartment(@PathVariable(value = "id") Integer id) {
//        //call delete department method
//        departmentService.deleteDepartmentById(id);
//        return "redirect:/department";
//    }
}
