package com.info.employee_details.controller;


import com.info.employee_details.dto.DesignationDto;
import com.info.employee_details.service.DesignationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

//passing param

@Controller
@RequestMapping("/designation")
public class DesignationController {

    @Autowired
    private DesignationService designationService;

    @GetMapping
    public String mainPage(Model model, String keyword, RedirectAttributes redirectAttributes) {

        if(model.getAttribute("keyword")!=null)
            if(keyword==null)
                keyword=model.getAttribute("keyword").toString();
        System.out.println(keyword);

        if (keyword!= null){
            System.out.println(keyword);
            if (keyword.equals("true") || keyword.equals("false")){
                System.out.println ("the status is:"+ keyword);
                model.addAttribute("designationDtoList",designationService.findByStatus(keyword));
            }
            //--------------------
            else
            {
                System.out.println("You have searched:"+ keyword);
                model.addAttribute("designationDtoList",designationService.findByKeyword(keyword));
            }

        }
//        else if (keyword!= null){
//            System.out.println("You have searched:"+ keyword);
//            model.addAttribute("designationDtoList",designationService.findByKeyword(keyword));
//        }

        else {
            model.addAttribute("designationDtoList", designationService.findAll());
        }

        if (model.getAttribute("designationDto") == null)
            model.addAttribute("designationDto", new DesignationDto());
        return "designation";
    }



    @PostMapping("/saveDesignation")
    public String saveDesignation(@ModelAttribute DesignationDto designationDto, RedirectAttributes redirectAttributes) {
        //  System.out.println(designationDto.getDshortcode());
        //call a service
        String message ="";
        designationDto=  designationService.saveDesignation(designationDto);

        if (designationDto==null){
            message= "Desigantion cannot be saved";
        }
        else
        {message ="Designation saved successfully";}

        redirectAttributes.addFlashAttribute ("message", message);
        return "redirect:/designation";
    }



    @GetMapping("/find-by-id/{id}")
    public String findById(@PathVariable("id") Integer designationid, RedirectAttributes redirectAttributes){
        DesignationDto designationDto= designationService.findById(designationid);
        if (designationDto!=null)
            redirectAttributes.addFlashAttribute("designationDto", designationDto);
        return "redirect:/designation";
    }



    @GetMapping("/find-by-keyword/{keyword}")
    public String findByKeyword(@PathVariable("keyword") String keyword,Model model, RedirectAttributes redirectAttributes)
    {
//        if (keyword.equals("true") || keyword.equals("false")){
//            System.out.println ("the status is:"+ keyword);
//            model.addAttribute("designationDtoList",designationService.findByStatus(keyword));
//        }
        //System.out.println(keyword); //ok
        redirectAttributes.addFlashAttribute("keyword",keyword);
        return "redirect:/designation";
    }



    @GetMapping("/delete/{id}")
    public String deleteDesignation(@PathVariable("id") Integer designationId){
        String message = "Designation deleted successfully";
        this.designationService.deleteDesignationById(designationId);
        return "redirect:/designation";
    }

    //-------------- ---  ----for parameters ------  -------------  --------------

    @GetMapping("/submit")
    public String submitForm(HttpServletRequest request, Model model)
    {   //String name =request.getParameter("name");
        //String shortcode = request.getParameter("shortcode");
        //String status =request.getParameter("status");
        String keyword =request.getParameter("keyword");
        //System.out.println(name);
        // System.out.println(shortcode);
        System.out.println(keyword);  //printed
        if (keyword != null) {
            model.addAttribute("designation", designationService.findByKeyword(keyword));  //value not passed here
        }
        return "redirect:/designation";
    }



    @PostMapping("/submit1")
    public String submitForm(@RequestParam("name") String name,
                             @RequestParam("shortcode") String shortcode,
                             @RequestParam("status") String status,
                             Model model, DesignationDto designationDto) {

        System.out.println(designationDto.getName());
        System.out.println(designationDto.getStatus());
        System.out.println(designationDto.getShortcode());
        model.addAttribute("d3", designationDto.getName());
        model.addAttribute("d4", designationDto.getShortcode());
        model.addAttribute("d5",designationDto.getStatus());
        return "redirect:/designation";
    }

    //------------------  -------------------  -------------------------  ------------  -------------  -----------
}
