package com.info.employee_details.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DesignationDto {
    private Integer id;
    private String name;
    private String shortcode;
    private Boolean status;
}
