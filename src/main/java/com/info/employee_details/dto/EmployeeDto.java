package com.info.employee_details.dto;

import com.info.employee_details.entity.Accessories;
import com.info.employee_details.entity.Department;
import com.info.employee_details.entity.Employee;
import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDto {
    private Integer id;
    private String fullName;
    private String address;
    private String emailId;
    private String phoneNo;
    private Integer age;
    private Integer departmentId;
    private List<Integer> accessoriesIdList;


    public EmployeeDto(Integer id, String fullName, String address, String emailId, String phoneNo, Integer age, Department department, List<Accessories> accessoriesList) {
    }

    public EmployeeDto(Employee employee){
        this.id=employee.getId();
        this.fullName=employee.getFullName();
        this.address = employee.getAddress();
        this.phoneNo=employee.getPhoneNo();
    }
}
