package com.info.employee_details.dto;

import com.info.employee_details.entity.Accessories;
import com.info.employee_details.enums.AccessoriesType;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccessoriesDto {
    private Integer id;
    private String accessoriesName;
    //    private String accessoriesType;
//    private String quantity;
    private String serialKey;
    private Boolean status;
    private AccessoriesType accessoriesEnum;
    private String description;
    private Boolean isActive;


    public AccessoriesDto(Accessories accessories){
        this.id = accessories.getId();
        this.accessoriesName = accessories.getAccessoriesName();
//        this.accessoriesType = accessories.getAccessoriesType();
//        this.quantity = accessories.getQuantity();
        this.serialKey = accessories.getSerialKey();
        this.status = accessories.getStatus();
        this.accessoriesEnum=accessories.getAccessoriesTypeEnum();
        this.description=accessories.getDescription();
        this.isActive=accessories.getIsActive();
    }


}
