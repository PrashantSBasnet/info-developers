package com.info.employee_details.dto;

import com.info.employee_details.entity.Department;
import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDto {
    private Integer id;
    private String departmentName;
    private String depCode;
    private Boolean isActive;
    private String contactPerson;
    private String contactNumber;



    public DepartmentDto(Department department){
        this.id = department.getId();
        this.departmentName = department.getDepartmentName();
        this.depCode = department.getDepCode();
        this.isActive = department.getIsActive();
        this.contactPerson = department.getContactPerson();
        this.contactNumber = department.getContactNumber();
    }
}
