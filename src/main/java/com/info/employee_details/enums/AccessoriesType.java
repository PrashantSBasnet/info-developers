package com.info.employee_details.enums;

import lombok.Getter;

public enum AccessoriesType {
    LAPTOP("Laptop"),
    MONITOR("Monitor"),
    MOUSE("Mouse"),
    KEYBOARD("KeyBoard"),
    CPU("CPU");

    @Getter
    private String displayType;

    AccessoriesType(String displayType){
        this.displayType=displayType;
    }
}
